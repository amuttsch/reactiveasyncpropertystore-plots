import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

x = [290, 26368, 95921, 409081]
y_EPK = [0.4523, 16.3929, 27.4808, 248.4873]
y_PKE = [0.4662, 16.4826, 28.4864, 249.0846]
y_RA_1 = [0.4386, 17.3455, 30.7018, 259.3324]
y_RA_4 = [0.2137, 5.9297, 10.0834, 118.3518]
y_RA_8 = [0.2087, 3.6349, 7.4915, 94.3800]
y_RA_16 = [0.2015, 2.9425, 5.9492, 68.0856]

fig, ax = plt.subplots()
ax.plot(x, y_EPK, label='EPKSequentailPS', marker='o')
ax.plot(x, y_PKE, label='PKESequentailPS', marker='o')
ax.plot(x, y_RA_1, label='ReactiveAsyncPS (1 thread)', marker='o')
ax.plot(x, y_RA_4, label='ReactiveAsyncPS (4 threads)', marker='o')
ax.plot(x, y_RA_8, label='ReactiveAsyncPS (8 threads)', marker='o')
ax.plot(x, y_RA_16, label='ReactiveAsyncPS (16 threads)', marker='o')

ax.set_xlabel('Number of entities in Project')
ax.set_ylabel('Runtime in s')
ax.set_xticks(x)
ax.set_xticklabels(x, rotation=40)
ax.set_xlim(0)
ax.set_ylim(0)
ax.legend(loc='upper left', prop={'size': 8})
plt.title("L2PurityAnalysis with lazy TAC")

plt.subplots_adjust(bottom=0.2)
plt.savefig('png/L2PurityAnalysis_RuntimeEntities.png', dpi=300)
#plt.show()

