import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np

data = np.genfromtxt('data/hist_thrownexceptions_dependencies.csv', delimiter=',', names=True)

fig = plt.figure()
fig.subplots_adjust(hspace=0.4)

plt.subplot(2, 1, 1)
plt.plot(data['numberDependencies'], data['total'], marker='.')
plt.xlim(xmin=0)
plt.ylim(ymin=0, ymax=500)
plt.xlabel('Number of Dependencies')
plt.ylabel('# Entities')
plt.title('L1ThrownExceptionsAnalysis')

plt.subplot(2, 1, 2)
plt.plot(data['numberDependencies'], data['total'], marker='.')
plt.xlim(xmin=0, xmax=25)
plt.ylim(ymin=0)
plt.xlabel('Number of Dependencies')
plt.xticks(np.arange(25+1))
for index, label in enumerate(plt.gca().get_xaxis().get_ticklabels()):
    if index % 5 != 0:
        label.set_visible(False)
plt.yticks([150000, 100000, 50000, 25000, 5000])
plt.ylabel('# Entities')
plt.subplots_adjust(left=0.15)

plt.savefig('png/L1ThrownExceptionsAnalysis_Histogram.png', dpi=300)
#plt.show()

