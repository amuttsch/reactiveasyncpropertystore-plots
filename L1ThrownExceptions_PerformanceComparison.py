import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

legend_all = ('Columbus', 'Groovy', 'Scala compiler', 'rt.jar')
data_columbus = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_Columbus.csv', delimiter=';', names=True)
data_groovy = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_Groovy.csv', delimiter=';', names=True)
data_scala = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_scala-compiler.csv', delimiter=';', names=True)
data_rt = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_rt.jar.csv', delimiter=';', names=True)


def gen_plot(file_suffix, legend, data, color=None):
    df = pd.DataFrame(data, index=data_columbus['Threads'])
    if file_suffix == '':
        df.plot.bar(width=1)
        plt.legend(legend, loc='upper left', prop={'size': 7})
    else:
        df.plot.bar(width=0.5, color=color)
        plt.legend(legend, loc='upper left', prop={'size': 13})

    plt.title("L1ThrownExceptionAnalysis")

    plt.xlabel('ReactiveAsyncPS Threads', horizontalalignment='right', x=0.7, labelpad=-20)
    plt.ylabel('Runtime in s')
    ticks = ['EPKSeqPS', 'PKESeqPS'] + list(map(lambda x: str(int(x)), data_groovy['Threads'][2:]))

    plt.gca().set_xticklabels(list(ticks), rotation=90)
    plt.subplots_adjust(bottom=0.2)

    plt.savefig('png/L1ThrownExceptionsAnalysis_PerformanceComparison' + file_suffix, dpi=300)
    #plt.show()


colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
gen_plot('', legend_all, np.c_[data_columbus['avgTimeInSeconds'], data_groovy['avgTimeInSeconds'], data_scala['avgTimeInSeconds'], data_rt['avgTimeInSeconds']])
gen_plot('_columbus', ('Columbus',), np.c_[data_columbus['avgTimeInSeconds']], colors[0])
gen_plot('_groovy', ('Groovy',), np.c_[data_groovy['avgTimeInSeconds']], colors[1])
gen_plot('_scala', ('Scala compiler',), np.c_[data_scala['avgTimeInSeconds']], colors[2])
gen_plot('_rt', ('rt.jar',), np.c_[data_rt['avgTimeInSeconds']], colors[3])