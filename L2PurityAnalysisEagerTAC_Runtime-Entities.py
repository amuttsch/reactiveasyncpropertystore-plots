import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

x = [290, 26368, 95921, 409081]
y_EPK = [0.0239, 0.7555, 2.9374, 34.9136]
y_PKE = [0.0238, 0.6467, 2.5609, 31.4266]
y_RA_1 = [0.0365, 1.4475, 5.8319, 43.9881]
y_RA_4 = [0.0145, 0.4687, 1.7823, 13.5129]
y_RA_8 = [0.0091, 0.2720, 1.0596, 7.2778]
y_RA_16 = [0.0079, 0.2292, 0.9524, 5.9744]

fig, ax = plt.subplots()
ax.plot(x, y_EPK, label='EPKSequentailPS', marker='o')
ax.plot(x, y_PKE, label='PKESequentailPS', marker='o')
ax.plot(x, y_RA_1, label='ReactiveAsyncPS (1 thread)', marker='o')
ax.plot(x, y_RA_4, label='ReactiveAsyncPS (4 threads)', marker='o')
ax.plot(x, y_RA_8, label='ReactiveAsyncPS (8 threads)', marker='o')
ax.plot(x, y_RA_16, label='ReactiveAsyncPS (16 threads)', marker='o')

ax.set_xlabel('Number of entities in Project')
ax.set_ylabel('Runtime in s')
ax.set_xticks(x)
ax.set_xticklabels(x, rotation=40)
ax.set_xlim(0)
ax.set_ylim(0)
ax.legend(loc='upper left', prop={'size': 8})
plt.title("L2PurityAnalysis with eager TAC")

plt.subplots_adjust(bottom=0.2)
plt.savefig('png/L2PurityAnalysisEagerTAC_RuntimeEntities.png', dpi=300)
#plt.show()

