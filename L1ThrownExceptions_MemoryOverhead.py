import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

x = ['EPKSequentialPS', 'PKESequentialPS', 'ReactiveAsyncPS']
y_total = [306, 278, 481]
y_entities = [223, 223, 223]
y_properties = [29, 29, 29]
y_overhead = list(map(lambda t, e, p: t - e - p, y_total, y_entities, y_properties))
y_ep = list(map(lambda e, p: e + p, y_entities, y_properties))

fig, ax = plt.subplots()
b_entities = ax.bar(x, y_entities, color='orange')
b_properties = ax.bar(x, y_properties, color='green', bottom=y_entities)
b_overhead = ax.bar(x, y_overhead, color='blue', bottom=y_ep)

ax.set_ylabel('Memory usage in MB')
ax.set_xticks(x)
plt.legend((b_overhead, b_properties, b_entities), ('PropertyStore Overhead', 'Properties', 'Entities'), loc='upper left')

labels = y_entities + y_properties + y_overhead

patches = ax.patches

for label, rect in zip(labels, patches):
    width = rect.get_width()
    if width > 0:
        x = rect.get_x()
        y = rect.get_y()
        height = rect.get_height()
        ax.text(x + width/2., y + height/2., str(label) + " MB", ha='center', va='center', color='white', fontweight='bold')

plt.savefig('png/L1ThrownExceptionsAnalysis_MemoryOverhead.png', dpi=300)
#plt.show()

