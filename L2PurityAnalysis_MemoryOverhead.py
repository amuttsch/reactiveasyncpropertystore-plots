import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

x = ['EPKSequentialPS', 'PKESequentialPS', 'ReactiveAsyncPS']
y_total = [2271, 2151, 2702]
y_entities = [248, 248, 248]
y_properties = [29, 29, 29]
y_tac = [1680, 1680, 1680]
y_overhead = list(map(lambda t, e, p, tac: t - e - p - tac, y_total, y_entities, y_properties, y_tac))
y_ep = list(map(lambda e, p: e + p, y_entities, y_properties))
y_ept = list(map(lambda e, p, tac: e + p + tac, y_entities, y_properties, y_tac))

fig, ax = plt.subplots()
b_entities = ax.bar(x, y_entities, color='orange')
b_properties = ax.bar(x, y_properties, color='green', bottom=y_entities)
b_tac = ax.bar(x, y_tac, color='red', bottom=y_ep)
b_overhead = ax.bar(x, y_overhead, color='blue', bottom=y_ept)

ax.set_ylabel('Memory usage in MB')
ax.set_xticks(x)
ax.set_ylim([0, 3200])
plt.legend((b_overhead, b_tac, b_properties, b_entities), ('PropertyStore Overhead', 'TAC', 'Properties', 'Entities'), loc='upper left')

labels = y_entities + y_properties + y_tac + y_overhead

patches = ax.patches

for label, rect in zip(labels, patches):
    width = rect.get_width()
    if width > 0:
        x = rect.get_x()
        y = rect.get_y()
        height = rect.get_height()
        ax.text(x + width/2., y + height/2., str(label) + " MB", ha='center', va='center', color='white', fontweight='bold')

plt.savefig('png/L2PurityAnalysis_MemoryOverhead.png', dpi=300)
#plt.show()

