import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np

x = [165, 19131, 55088, 73894, 142425, 160649, 234088]
y_EPK = [5.4, 40.04, 97.93, 135.04, 266.1, 306.38, 486.06]
y_PKE = [5.92, 36.81, 90.06, 123.2, 241.65, 278.57, 448.47]
y_RA = [6.11, 60.59, 158.33, 213.41, 418.2, 481.16, 740.57]

fig, ax = plt.subplots()
ax.plot(x, y_EPK, label='EPKSequentailPS', marker='o')
ax.plot(x, y_PKE, label='PKESequentailPS', marker='o')
ax.plot(x, y_RA, label='ReactiveAsyncPS', marker='o')

ax.set_xlabel('Number of methods analysed')
ax.set_ylabel('Memory usage in MB')
ax.set_xticks(x)
ax.set_xticklabels(x, rotation=40)
ax.set_xlim(0)
ax.set_ylim(0)
ax.legend(loc='lower right')

plt.subplots_adjust(bottom=0.2)
plt.savefig('png/L1ThrownExceptionsAnalysis_MemoryComparison.png', dpi=300)
#plt.show()
