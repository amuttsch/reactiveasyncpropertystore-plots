import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

x = ['EPKSequentialPS', 'PKESequentialPS', 'ReactiveAsyncPS']
y = [2271, 2151, 2702]

fig, ax = plt.subplots()
b = ax.bar(x, y, color='blue')
ax.set_ylabel('Memory in MB')

def autolabel(rects):
    # attach some text labels
    for rect, l in zip(rects, y):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height / 2.1,
                f'{l} MB', color='white', fontweight='bold',
                ha='center', va='bottom')

autolabel(b)

plt.savefig('png/L2PurityAnalysis_Memory.png', dpi=300)

