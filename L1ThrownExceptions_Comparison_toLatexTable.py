import matplotlib
matplotlib.use('Qt5Agg')
import numpy as np

legend = ('Columbus', 'Groovy', 'Scala compiler', 'rt.jar')

data_columbus = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_Columbus.csv', delimiter=';', names=True)
data_groovy = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_Groovy.csv', delimiter=';', names=True)
data_scala = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_scala-compiler.csv', delimiter=';', names=True)
data_rt = np.genfromtxt('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_rt.jar.csv', delimiter=';', names=True)

def toStr(x):
    return '% .3f' % x

print('& \\multicolumn{2}{c}{\\textbf{Sequential}} & \\multicolumn{17}{c}{\\textbf{ReactiveAsync}} \\\\')
print('\\textbf{Project} & PKE & EPK & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 & 13 & 14 & 15 & 16 & 17 \\\\')
print('\\addlinespace[1ex] ')

print('Columbus & ' + ' & '.join(map(toStr, data_columbus['avgTimeInSeconds'].tolist())) + '\\\\')
print('Groovy & ' + ' & '.join(map(toStr, data_groovy['avgTimeInSeconds'].tolist())) + '\\\\')
print('Scala compiler & ' + ' & '.join(map(toStr, data_scala['avgTimeInSeconds'].tolist())) + '\\\\')
print('rt.jar & ' + ' & '.join(map(toStr, data_rt['avgTimeInSeconds'].tolist())) + '\\\\')
