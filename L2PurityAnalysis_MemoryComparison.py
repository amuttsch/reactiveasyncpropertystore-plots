import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

x = [290, 26368, 66196, 95921, 180227, 262885, 377526]
y_EPK = [8.49, 262.33, 388.66, 683.4, 1350.01, 2272.65, 3761.58]
y_PKE = [8.97, 248.17, 352.37, 659.44, 1285.96, 2151.63, 3544.07]
y_RA = [9.62, 313.90, 532.12, 803.11, 1634.04, 2703.14, 4469.32]

fig, ax = plt.subplots()
ax.plot(x, y_EPK, label='EPKSequentailPS', marker='o')
ax.plot(x, y_PKE, label='PKESequentailPS', marker='o')
ax.plot(x, y_RA, label='ReactiveAsyncPS', marker='o')

ax.set_xlabel('Number of entities analysed')
ax.set_ylabel('Memory usage in MB')
ax.set_xticks(x)
ax.set_xticklabels(x, rotation=40)
ax.set_xlim(0)
ax.set_ylim(0)
ax.legend(loc='lower right')

plt.subplots_adjust(bottom=0.2)
plt.savefig('png/L2PurityAnalysis_MemoryComparison.png', dpi=300)
#plt.show()
