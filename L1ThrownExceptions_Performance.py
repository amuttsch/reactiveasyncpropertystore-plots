import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np

with open('data/PropertyStoreEvaluationResults_L1ThrownExceptionsAnalysis_rt.jar.csv', 'rb') as f_in:
    lines = f_in.readlines()
    lines_EPK = filter(lambda x: b'EPK' in x or b'Threads' in x, lines)
    lines_PKE = filter(lambda x: b'PKE' in x or b'Threads' in x, lines)
    lines_RA = filter(lambda x: b'ReactiveAsyncPropertyStore' in x or b'Threads' in x, lines)
    data_EPK = np.genfromtxt(lines_EPK, delimiter=';', names=True)
    data_PKE = np.genfromtxt(lines_PKE, delimiter=';', names=True)
    data_RA = np.genfromtxt(lines_RA, delimiter=';', names=True)

    y_EPK = 17 * [data_EPK['avgTimeInSeconds']]
    y_PKE = 17 * [data_PKE['avgTimeInSeconds']]
    y_RA = data_RA['avgTimeInSeconds']

    fig, ax = plt.subplots()
    ax.plot(data_RA['Threads'], y_EPK, label='EPKSequentailPS')
    ax.plot(data_RA['Threads'], y_PKE, label='PKESequentailPS')
    ax.plot(data_RA['Threads'], y_RA, label='ReactiveAsyncPS', marker='o')

    plt.title("L1ThrownExceptionAnalysis")
    ax.set_xlabel('Threads')
    ax.set_ylabel('Runtime in s')
    ax.set_ylim([4, 16])
    ax.legend(loc='lower right')

    plt.axvline(x=8, linestyle='--', color='grey', linewidth=1, dashes=(5, 20))
    plt.text(8.2, 15, '# physical cores', fontsize=8, color='grey')

    plt.savefig('png/L1ThrownExceptionsAnalysis_Performance.png', dpi=300)
    #plt.show()

