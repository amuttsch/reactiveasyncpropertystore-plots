import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

x = [165, 19131, 73894, 253956]
y_EPK = [0.0173, 0.7201, 2.4774, 8.4595]
y_PKE = [0.0226, 0.7282, 2.3015, 7.3240]
y_RA_1 = [0.0227, 1.3069, 3.2856, 10.6824]
y_RA_4 = [0.0197, 1.5722, 5.4914, 12.6576]
y_RA_8 = [0.0198, 1.6312, 5.5730, 12.8334]
y_RA_16 = [0.0212, 1.7651, 5.7778, 13.2548]

fig, ax = plt.subplots()
ax.plot(x, y_EPK, label='EPKSequentailPS', marker='o')
ax.plot(x, y_PKE, label='PKESequentailPS', marker='o')
ax.plot(x, y_RA_1, label='ReactiveAsyncPS (1 thread)', marker='o')
ax.plot(x, y_RA_4, label='ReactiveAsyncPS (4 threads)', marker='o')
ax.plot(x, y_RA_8, label='ReactiveAsyncPS (8 threads)', marker='o')
ax.plot(x, y_RA_16, label='ReactiveAsyncPS (16 threads)', marker='o')

ax.set_xlabel('Number of entities in Project')
ax.set_ylabel('Runtime in s')
ax.set_xticks(x)
ax.set_xticklabels(x, rotation=40)
ax.set_xlim(0)
ax.set_ylim(0)
ax.legend(loc='upper left', prop={'size': 8})
plt.title("L1ThrownExceptionsAnalysis")

plt.subplots_adjust(bottom=0.2)
plt.savefig('png/L1ThrownExceptionsAnalysis_RuntimeEntities.png', dpi=300)
#plt.show()

