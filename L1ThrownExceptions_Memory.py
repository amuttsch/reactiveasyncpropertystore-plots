import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np

x = ['EPKSequentialPS', 'PKESequentialPS', 'ReactiveAsyncPS']
y = [306.38, 278.57, 481.16]

fig, ax = plt.subplots()
b = ax.bar(x, y, color='blue')
ax.set_ylabel('Memory in MB')

def autolabel(rects):
    # attach some text labels
    for rect, l in zip(rects, y):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height / 2.1,
                f'{l} MB', color='white', fontweight='bold',
                ha='center', va='bottom')

autolabel(b)

plt.savefig('png/L1ThrownExceptionsAnalysis_Memory.png', dpi=300)

