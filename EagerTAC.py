import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np

data = np.genfromtxt('data/EagerTAC.csv', delimiter=';', names=True)
x = ['Columbus', 'Groovy', 'Scala compiler', 'rt.jar']
y = data['timeInSecondsWith8Threads']

fig, ax = plt.subplots()
b = ax.bar(x, y, color='blue')
ax.set_ylabel('Runtime in s')
plt.title('Eager TAC conversion')

for p in ax.patches:
    ax.annotate("%.2f s" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
        ha='center', va='center', fontsize=11, color='gray', xytext=(0, 10),
        textcoords='offset points')
_ = ax.set_ylim(0, 45)

plt.savefig('png/EagerTAC.png', dpi=300)

