import matplotlib
matplotlib.use('Qt5Agg')
import numpy as np

legend = ('Columbus', 'Groovy', 'Scala compiler', 'rt.jar')
eager_data_columbus = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysisEagerTAC_Columbus.csv', delimiter=';', names=True)
eager_data_groovy = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysisEagerTAC_Groovy.csv', delimiter=';', names=True)
eager_data_scala = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysisEagerTAC_scala-compiler.csv', delimiter=';', names=True)
eager_data_rt = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysisEagerTAC_rt.jar.csv', delimiter=';', names=True)

data_columbus = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysis_Columbus.csv', delimiter=';', names=True)
data_groovy = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysis_Groovy.csv', delimiter=';', names=True)
data_scala = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysis_scala-compiler.csv', delimiter=';', names=True)
data_rt = np.genfromtxt('data/PropertyStoreEvaluationResults_L2PurityAnalysis_rt.jar.csv', delimiter=';', names=True)

def toStr(x):
    return '% .3f' % x

print('& & \\multicolumn{2}{c}{\\textbf{Sequential}} & \\multicolumn{17}{c}{\\textbf{ReactiveAsync}} \\\\')
print('\\textbf{Project} & \\textbf{TAC} & PKE & EPK & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 & 13 & 14 & 15 & 16 & 17 \\\\')
print('\\addlinespace[1ex] ')

print('Columbus & eager & ' + ' & '.join(map(toStr, eager_data_columbus['avgTimeInSeconds'].tolist())) + '\\\\')
print('Groovy & eager & ' + ' & '.join(map(toStr, eager_data_groovy['avgTimeInSeconds'].tolist())) + '\\\\')
print('Scala compiler & eager & ' + ' & '.join(map(toStr, eager_data_scala['avgTimeInSeconds'].tolist())) + '\\\\')
print('rt.jar & eager & ' + ' & '.join(map(toStr, eager_data_rt['avgTimeInSeconds'].tolist())) + '\\\\')

print('Columbus & lazy & ' + ' & '.join(map(toStr, data_columbus['avgTimeInSeconds'].tolist())) + '\\\\')
print('Groovy & lazy & ' + ' & '.join(map(toStr, data_groovy['avgTimeInSeconds'].tolist())) + '\\\\')
print('Scala compiler & lazy & ' + ' & '.join(map(toStr, data_scala['avgTimeInSeconds'].tolist())) + '\\\\')
print('rt.jar & lazy & ' + ' & '.join(map(toStr, data_rt['avgTimeInSeconds'].tolist())) + '\\\\')
